/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    LEDS.C
 *      Purpose: Microprocessors Laboratory
 *----------------------------------------------------------------------------
 *      
 *      Author: Pawel Russek AGH University of Science and Technology
 *---------------------------------------------------------------------------*/

#include "leds.h"												//Declarations


/*----------------------------------------------------------------------------
  Function that initializes LEDs
 *----------------------------------------------------------------------------*/

void welcomeLeds() {
	volatile int delay;

	//Welcome sequence
	for(delay=0; delay<1200000; delay++);
	LED_RED_ON();
	LED_GREEN_ON();
	for(delay=0; delay<1200000; delay++);
	LED_RED_TOGGLE();
	LED_GREEN_TOGGLE();
	for(delay=0; delay<1200000; delay++);
	LED_RED_TOGGLE();
	LED_GREEN_TOGGLE();
	for(delay=0; delay<1200000; delay++);
	LED_RED_TOGGLE();
	LED_GREEN_TOGGLE();
}



