/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "SerialComm.h"

/*!
 * @brief Application entry point.
 */
int main(void) {
  /* Init board hardware. */
  BOARD_InitPins();
  BOARD_BootClockRUN();
  BOARD_InitDebugConsole();

  LED_GREEN_INIT(0);
  LED_GREEN_OFF();

  LED_RED_INIT(0);
  LED_RED_OFF();

  uartInitialize();
  initChannels();

  /* Add your code here */
  comm_mode_t currMode = IDLE;
  comm_config_state_t currState = IDLE;
  uint8_t data[10];
  for(;;) { /* Infinite loop to avoid leaving the main function */
	  switch (currMode) {
		case IDLE:
			LED_GREEN_ON();
			LED_RED_OFF();
			UART_ReadBlocking(UART0_COMPAT, data, 1);
			currMode = changeMode(data[0]);
			break;

		case CONFIG_MODE:
			LED_GREEN_OFF();
			LED_RED_ON();
			UART_ReadBlocking(UART0_COMPAT, data, 1);
			currState = changeConfigMode(data[0]);
			break;
			switch (currState) {
				case REPLY_CMD:
					break;

				case SET_CONFIG:
					break;

				case ERR_UNKNOWN_CONFIG:
					currMode = ERR_MODE;

			}
		case SAMPLING_MODE:
			sampleChannels();

		case TRANSFER_MODE:
			LED_GREEN_OFF();
			LED_RED_ON();
			for (int chNum = 0; chNum < 8; ++chNum) {
				UART_WriteBlocking(UART0_COMPAT, dataBuffer[chNum], samplingDuration);
			}
			currMode = IDLE;
			break;

		case ERR_MODE:
			UART_WriteBlocking(UART0_COMPAT, err_msg, 3);
			currMode = IDLE;
			break;
		default:
			currMode = IDLE;
			break;
	}
  }
}
